package com.example.lebiscuit.ui.Regulamentos;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.lebiscuit.CadastroActivity;
import com.example.lebiscuit.Minha_Le;
import com.example.lebiscuit.PoliticaPrivacidade;
import com.example.lebiscuit.R;
import com.example.lebiscuit.Regulamentos;
import com.example.lebiscuit.TermosAcitiviy;


public class RegulamentosFragment extends Fragment {

    private ImageView buttonHome;
    private ImageView buttonTermosuso;
    private ImageView buttonMinhale;
    private ImageView buttonPoliticaPrivade;

    private RegulamentosViewModel regulamentos;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        regulamentos =
                ViewModelProviders.of(this).get(RegulamentosViewModel.class);
        View root = inflater.inflate(R.layout.activity_regulamentos, container, false);


        //Metudo Para Transferir Tela * Regulamentos - Cadastro
        buttonHome = root.findViewById(R.id.Buttonhome);
        buttonHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), CadastroActivity.class);
                startActivity(intent);
            }
        });//Fim Metuodo

        //Metudo Para Transferir Tela * Regulamentos - Termos De Uso
        buttonTermosuso = root.findViewById(R.id.button_termos_de_Uso);
        buttonTermosuso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), TermosAcitiviy.class);
                startActivity(intent);
            }
        });//Fim Metuodo

        //Metudo Para Transferir Tela * Regulamentos - Minha Le
        buttonMinhale = findViewById(R.id.Button_MinhaLe);
        buttonMinhale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Regulamentos.this, Minha_Le.class);
                startActivity(intent);
            }
        });//Fim Metuodo

        //Metudo Para Transferir Tela * Regulamentos - Politica De Privacidade
        buttonPoliticaPrivade = findViewById(R.id.ButtonPolitica);
        buttonPoliticaPrivade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Regulamentos.this, PoliticaPrivacidade.class);
                startActivity(intent);
            }
        });//Fim Metuodo

        return root;
    }
}
