package com.example.lebiscuit.ui.Cadastro;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.example.lebiscuit.R;


public class CadastroFragament extends Fragment {


    private CadastroViewModel cadastro;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        cadastro =
                ViewModelProviders.of(this).get(CadastroViewModel.class);
        View root = inflater.inflate(R.layout.activiy_cadastro, container, false);

        return root;
    }
}
