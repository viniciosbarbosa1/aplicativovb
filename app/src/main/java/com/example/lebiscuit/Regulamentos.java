package com.example.lebiscuit;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;


public class Regulamentos extends AppCompatActivity {

    private ImageView buttonHome;
    private ImageView buttonTermosuso;
    private ImageView buttonMinhale;
    private ImageView buttonPoliticaPrivade;

    @Override

    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_regulamentos);

        //Metudo Para Transferir Tela * Regulamentos - Cadastro
        buttonHome = findViewById(R.id.Buttonhome);
        buttonHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Regulamentos.this, CadastroActivity.class);
                startActivity(intent);
            }
        });//Fim Metuodo

        //Metudo Para Transferir Tela * Regulamentos - Termos De Uso
        buttonTermosuso = findViewById(R.id.button_termos_de_Uso);
        buttonTermosuso.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Regulamentos.this, TermosAcitiviy.class);
                startActivity(intent);
            }
        });//Fim Metuodo

        //Metudo Para Transferir Tela * Regulamentos - Minha Le
        buttonMinhale = findViewById(R.id.Button_MinhaLe);
        buttonMinhale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Regulamentos.this, Minha_Le.class);
                startActivity(intent);
            }
        });//Fim Metuodo

        //Metudo Para Transferir Tela * Regulamentos - Politica De Privacidade
        buttonPoliticaPrivade = findViewById(R.id.ButtonPolitica);
        buttonPoliticaPrivade.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Regulamentos.this, PoliticaPrivacidade.class);
                startActivity(intent);
            }
        });//Fim Metuodo





    }

}
