package com.example.lebiscuit;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.lebiscuit.ui.Home.HomeOptions;

public class PoliticaPrivacidade extends AppCompatActivity {


    private ImageView ButtonRegulatorio;
    private Button Buttonvoltar;
    private Button ButtonAceitar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_politica_privacidade);

        ButtonRegulatorio = findViewById(R.id.ButtonVoltarPo);
        ButtonRegulatorio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PoliticaPrivacidade.this, Regulamentos.class);
                startActivity(intent);
            }
        });

        Buttonvoltar = findViewById(R.id.ButtonVoltarPolitica);
        Buttonvoltar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PoliticaPrivacidade.this, Regulamentos.class);
                startActivity(intent);
            }
        });

        ButtonAceitar = findViewById(R.id.button_AceitarPolitica);
        ButtonAceitar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PoliticaPrivacidade.this, HomeOptions.class);
                startActivity(intent);
            }
        });


    }

}
